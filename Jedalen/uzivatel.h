﻿#pragma once
#include <QtWidgets/QMainWindow>
#include <QDialog>
#include "ui_uzivatel.h"
#include "ui_jedalen.h"
#include "jedalen.h"
#include "prihlasenie.h"

class Uzivatel : public QDialog {
	Q_OBJECT

public:
	Uzivatel(QWidget * parent = Q_NULLPTR);
	~Uzivatel();
	
	public slots:
	void click9();

private:
	Ui::Uzivatel ui;
	QStringList mena;
	QStringList hesla;
	QStringList peniaze;
};


