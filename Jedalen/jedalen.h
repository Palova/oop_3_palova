#ifndef JEDALEN_H
#define JEDALEN_H

#include <QtWidgets/QMainWindow>
#include "ui_jedalen.h"
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QListWidget>
#include <QStringList>
#include <string>
#include <qmessagebox.h>
#include <QListWidgetItem>
#include "prihlasenie.h"
#include "admin.h"

class Jedalen : public QMainWindow
{
	Q_OBJECT

public:
	Jedalen(QWidget *parent = 0);
	~Jedalen();

	public slots:
	void click2();
	void click3();
	void click4();
	void click5();
	void click7();
	void click8();
	void click9();
	

private:
	Ui::JedalenClass ui;
		
	QStringList vybrane;
	QStringList den;
	QStringList nazov;
	QStringList cena;
	QString vyberMeno;
	QString vyberPeniaze;
	QStringList peniaze;
	int indexPouzivatel;
};

#endif // JEDALEN_H
