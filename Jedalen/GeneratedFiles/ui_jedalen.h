/********************************************************************************
** Form generated from reading UI file 'jedalen.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JEDALEN_H
#define UI_JEDALEN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_JedalenClass
{
public:
    QAction *actionSuborsjedlami;
    QAction *actionVymazanie_Zoznamu;
    QAction *actionVypisat_fakturu;
    QWidget *centralWidget;
    QListWidget *listWidget;
    QPushButton *pushButton;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QLabel *label;
    QLabel *label_2;
    QPushButton *pushButton_5;
    QListWidget *listWidget_2;
    QLineEdit *lineEdit;
    QMenuBar *menuBar;
    QMenu *menuUzivatelia;
    QMenu *menuVypis;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *JedalenClass)
    {
        if (JedalenClass->objectName().isEmpty())
            JedalenClass->setObjectName(QStringLiteral("JedalenClass"));
        JedalenClass->resize(674, 547);
        actionSuborsjedlami = new QAction(JedalenClass);
        actionSuborsjedlami->setObjectName(QStringLiteral("actionSuborsjedlami"));
        actionVymazanie_Zoznamu = new QAction(JedalenClass);
        actionVymazanie_Zoznamu->setObjectName(QStringLiteral("actionVymazanie_Zoznamu"));
        actionVypisat_fakturu = new QAction(JedalenClass);
        actionVypisat_fakturu->setObjectName(QStringLiteral("actionVypisat_fakturu"));
        centralWidget = new QWidget(JedalenClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        listWidget = new QListWidget(centralWidget);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setGeometry(QRect(10, 10, 461, 281));
        listWidget->setSelectionMode(QAbstractItemView::MultiSelection);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(530, 60, 93, 28));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QStringLiteral("pushButton_3"));
        pushButton_3->setGeometry(QRect(530, 100, 93, 28));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 300, 93, 28));
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 350, 121, 41));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 390, 241, 61));
        pushButton_5 = new QPushButton(centralWidget);
        pushButton_5->setObjectName(QStringLiteral("pushButton_5"));
        pushButton_5->setGeometry(QRect(510, 410, 131, 28));
        listWidget_2 = new QListWidget(centralWidget);
        listWidget_2->setObjectName(QStringLiteral("listWidget_2"));
        listWidget_2->setGeometry(QRect(130, 360, 101, 21));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(250, 410, 111, 22));
        JedalenClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(JedalenClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 674, 26));
        menuUzivatelia = new QMenu(menuBar);
        menuUzivatelia->setObjectName(QStringLiteral("menuUzivatelia"));
        menuVypis = new QMenu(menuBar);
        menuVypis->setObjectName(QStringLiteral("menuVypis"));
        JedalenClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(JedalenClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        JedalenClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(JedalenClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        JedalenClass->setStatusBar(statusBar);

        menuBar->addAction(menuUzivatelia->menuAction());
        menuBar->addAction(menuVypis->menuAction());
        menuUzivatelia->addAction(actionSuborsjedlami);
        menuUzivatelia->addAction(actionVymazanie_Zoznamu);
        menuVypis->addAction(actionVypisat_fakturu);

        retranslateUi(JedalenClass);
        QObject::connect(pushButton, SIGNAL(clicked()), JedalenClass, SLOT(click2()));
        QObject::connect(actionSuborsjedlami, SIGNAL(triggered()), JedalenClass, SLOT(click3()));
        QObject::connect(actionVymazanie_Zoznamu, SIGNAL(triggered()), JedalenClass, SLOT(click4()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), JedalenClass, SLOT(click5()));
        QObject::connect(actionVypisat_fakturu, SIGNAL(triggered()), JedalenClass, SLOT(click7()));
        QObject::connect(pushButton_3, SIGNAL(clicked()), JedalenClass, SLOT(click8()));
        QObject::connect(pushButton_5, SIGNAL(clicked()), JedalenClass, SLOT(click9()));

        QMetaObject::connectSlotsByName(JedalenClass);
    } // setupUi

    void retranslateUi(QMainWindow *JedalenClass)
    {
        JedalenClass->setWindowTitle(QApplication::translate("JedalenClass", "Jedalen", 0));
        actionSuborsjedlami->setText(QApplication::translate("JedalenClass", "Subor s Jedlami", 0));
        actionVymazanie_Zoznamu->setText(QApplication::translate("JedalenClass", "Vymazanie", 0));
        actionVypisat_fakturu->setText(QApplication::translate("JedalenClass", "Vypisat fakturu", 0));
        pushButton->setText(QApplication::translate("JedalenClass", "Prihlasenie", 0));
        pushButton_3->setText(QApplication::translate("JedalenClass", "Odhlasenie", 0));
        pushButton_2->setText(QApplication::translate("JedalenClass", "Vyber Jedal", 0));
        label->setText(QApplication::translate("JedalenClass", "V\303\241\305\241 aktu\303\241lny kredit:", 0));
        label_2->setText(QApplication::translate("JedalenClass", "Zvolte sumu za ktor\303\272 chcete dobit kredit:", 0));
        pushButton_5->setText(QApplication::translate("JedalenClass", "Dobit kredit", 0));
        menuUzivatelia->setTitle(QApplication::translate("JedalenClass", "Jedla", 0));
        menuVypis->setTitle(QApplication::translate("JedalenClass", "Vypis", 0));
    } // retranslateUi

};

namespace Ui {
    class JedalenClass: public Ui_JedalenClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JEDALEN_H
