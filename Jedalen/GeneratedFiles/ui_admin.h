/********************************************************************************
** Form generated from reading UI file 'admin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ADMIN_H
#define UI_ADMIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Admin
{
public:
    QLabel *label;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton_2;

    void setupUi(QDialog *Admin)
    {
        if (Admin->objectName().isEmpty())
            Admin->setObjectName(QStringLiteral("Admin"));
        Admin->resize(476, 138);
        label = new QLabel(Admin);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(20, 30, 121, 16));
        comboBox = new QComboBox(Admin);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(150, 30, 141, 22));
        pushButton = new QPushButton(Admin);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(320, 30, 93, 28));
        label_2 = new QLabel(Admin);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(20, 80, 121, 16));
        lineEdit = new QLineEdit(Admin);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setGeometry(QRect(150, 80, 151, 22));
        pushButton_2 = new QPushButton(Admin);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));
        pushButton_2->setGeometry(QRect(330, 80, 93, 28));

        retranslateUi(Admin);
        QObject::connect(pushButton, SIGNAL(clicked()), Admin, SLOT(click10()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), Admin, SLOT(click11()));

        QMetaObject::connectSlotsByName(Admin);
    } // setupUi

    void retranslateUi(QDialog *Admin)
    {
        Admin->setWindowTitle(QApplication::translate("Admin", "Admin", 0));
        label->setText(QApplication::translate("Admin", "Vymazanie Uzivatela", 0));
        pushButton->setText(QApplication::translate("Admin", "Vymaz", 0));
        label_2->setText(QApplication::translate("Admin", "Pridanie Uzivatela", 0));
        pushButton_2->setText(QApplication::translate("Admin", "Pridaj", 0));
    } // retranslateUi

};

namespace Ui {
    class Admin: public Ui_Admin {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ADMIN_H
