/********************************************************************************
** Form generated from reading UI file 'uzivatel.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_UZIVATEL_H
#define UI_UZIVATEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Uzivatel
{
public:
    QLabel *label;
    QLabel *label_2;
    QComboBox *comboBox;
    QPushButton *pushButton;
    QComboBox *comboBox_2;

    void setupUi(QDialog *Uzivatel)
    {
        if (Uzivatel->objectName().isEmpty())
            Uzivatel->setObjectName(QStringLiteral("Uzivatel"));
        Uzivatel->resize(375, 168);
        label = new QLabel(Uzivatel);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 10, 121, 41));
        label_2 = new QLabel(Uzivatel);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 30, 241, 61));
        comboBox = new QComboBox(Uzivatel);
        comboBox->setObjectName(QStringLiteral("comboBox"));
        comboBox->setGeometry(QRect(260, 50, 51, 22));
        pushButton = new QPushButton(Uzivatel);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(120, 100, 131, 28));
        comboBox_2 = new QComboBox(Uzivatel);
        comboBox_2->setObjectName(QStringLiteral("comboBox_2"));
        comboBox_2->setGeometry(QRect(130, 20, 73, 22));

        retranslateUi(Uzivatel);
        QObject::connect(pushButton, SIGNAL(clicked()), Uzivatel, SLOT(click7()));

        QMetaObject::connectSlotsByName(Uzivatel);
    } // setupUi

    void retranslateUi(QDialog *Uzivatel)
    {
        Uzivatel->setWindowTitle(QApplication::translate("Uzivatel", "Uzivatel", 0));
        label->setText(QApplication::translate("Uzivatel", "V\303\241\305\241 aktu\303\241lny kredit:", 0));
        label_2->setText(QApplication::translate("Uzivatel", "Zvolte sumu za ktor\303\272 chcete dobit kredit:", 0));
        comboBox->clear();
        comboBox->insertItems(0, QStringList()
         << QApplication::translate("Uzivatel", "5\342\202\254", 0)
         << QApplication::translate("Uzivatel", "10\342\202\254", 0)
         << QApplication::translate("Uzivatel", "15\342\202\254", 0)
         << QApplication::translate("Uzivatel", "20\342\202\254", 0)
         << QApplication::translate("Uzivatel", "25\342\202\254", 0)
         << QApplication::translate("Uzivatel", "30\342\202\254", 0)
         << QString()
        );
        pushButton->setText(QApplication::translate("Uzivatel", "Dobit kredit", 0));
    } // retranslateUi

};

namespace Ui {
    class Uzivatel: public Ui_Uzivatel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_UZIVATEL_H
