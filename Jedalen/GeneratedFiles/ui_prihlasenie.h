/********************************************************************************
** Form generated from reading UI file 'prihlasenie.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PRIHLASENIE_H
#define UI_PRIHLASENIE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDialog>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Prihlasenie
{
public:
    QFormLayout *formLayout;
    QLabel *label;
    QLabel *label_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QComboBox *comboBox;

    void setupUi(QDialog *Prihlasenie)
    {
        if (Prihlasenie->objectName().isEmpty())
            Prihlasenie->setObjectName(QStringLiteral("Prihlasenie"));
        Prihlasenie->resize(254, 125);
        formLayout = new QFormLayout(Prihlasenie);
        formLayout->setSpacing(6);
        formLayout->setContentsMargins(11, 11, 11, 11);
        formLayout->setObjectName(QStringLiteral("formLayout"));
        label = new QLabel(Prihlasenie);
        label->setObjectName(QStringLiteral("label"));

        formLayout->setWidget(0, QFormLayout::LabelRole, label);

        label_2 = new QLabel(Prihlasenie);
        label_2->setObjectName(QStringLiteral("label_2"));

        formLayout->setWidget(1, QFormLayout::LabelRole, label_2);

        lineEdit = new QLineEdit(Prihlasenie);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));
        lineEdit->setEchoMode(QLineEdit::Password);

        formLayout->setWidget(1, QFormLayout::FieldRole, lineEdit);

        pushButton = new QPushButton(Prihlasenie);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        formLayout->setWidget(2, QFormLayout::FieldRole, pushButton);

        comboBox = new QComboBox(Prihlasenie);
        comboBox->setObjectName(QStringLiteral("comboBox"));

        formLayout->setWidget(0, QFormLayout::FieldRole, comboBox);


        retranslateUi(Prihlasenie);
        QObject::connect(pushButton, SIGNAL(clicked()), Prihlasenie, SLOT(click1()));

        QMetaObject::connectSlotsByName(Prihlasenie);
    } // setupUi

    void retranslateUi(QDialog *Prihlasenie)
    {
        Prihlasenie->setWindowTitle(QApplication::translate("Prihlasenie", "Prihlasenie", 0));
        label->setText(QApplication::translate("Prihlasenie", "Meno", 0));
        label_2->setText(QApplication::translate("Prihlasenie", "Heslo", 0));
        pushButton->setText(QApplication::translate("Prihlasenie", "OK", 0));
    } // retranslateUi

};

namespace Ui {
    class Prihlasenie: public Ui_Prihlasenie {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PRIHLASENIE_H
