#include "jedalen.h"


Jedalen::Jedalen(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
}

Jedalen::~Jedalen()
{

}


void Jedalen::click2()    //otvorenie okna pre prihlasenie
{
	Prihlasenie login;
	login.exec();
	vyberMeno = login.vyberMeno;
	indexPouzivatel = login.indexPouzivatel;
	peniaze = login.peniaze;
	vyberPeniaze = peniaze.at(indexPouzivatel);
	ui.listWidget_2->addItem(vyberPeniaze);
	
}


void Jedalen::click3()   //nacitanie jedal
{
	QString fileName = QFileDialog::getOpenFileName(this, "Otvorit subor", "", "");


	QFile data(fileName);
	QTextStream input(&data);
	if (!data.open(QFile::ReadOnly))     //kontrola otvorenia suboru
	{
		return;
	}
	for (int i = 1; !input.atEnd(); i++)
	{
		den << input.readLine();
		nazov << input.readLine();
		cena << input.readLine();

		ui.listWidget->addItems(den);
		den.clear();
		ui.listWidget->addItems(nazov);
		nazov.clear();
		ui.listWidget->addItems(cena);
		cena.clear();
	}

	
}

void Jedalen::click4()                    //vymazanie zoznamu jedal
{
	ui.listWidget->clear();
}

void Jedalen::click5()                   //vybratie jedal
{
	QMessageBox mbox;
	
	
	

	for (int i = 0; i < ui.listWidget->selectedItems().count(); i++)
	{
	vybrane << ui.listWidget->selectedItems().at(i)->text();
	
	}
	ui.listWidget->clear();
	ui.listWidget->addItems(vybrane);

}

void Jedalen::click7()                //vypis
{
	QMessageBox mbox;
	QFile output("Vypis.txt");
	if (output.open(QFile::WriteOnly))
	{
		QTextStream out(&output);
		QString text = "Faktura:";
		QString text2 = "Jedalen Eat and Meat, Hornooravska 14, 845 45 Bratislava";

		out << text << "\r\n";
		out << text2 << "\r\n";
		out << "Pouzivatel: " << vyberMeno << "\r\n";
		for (int i=0; i < vybrane.size(); i++)
		{
			
			out << vybrane.at(i) << "\r\n";
		}
		out << "Dakujeme za navstevu" << "\r\n";
		output.close();
	}
	
		mbox.setText("Vasa faktura bola vydana");
		mbox.exec();
}

void Jedalen::click8()                        //odhlasenie
{
	QMessageBox mbox;
	ui.listWidget->clear();
	ui.listWidget_2->clear();
	ui.lineEdit->clear();
	mbox.setText("Odhlasenie uspesne");
	mbox.exec();
	
}

void Jedalen::click9()                 // dobitie kreditu
{

	ui.listWidget_2->clear();
	
	
	
	ui.listWidget_2->addItem(QString::number(vyberPeniaze.toDouble()+ ui.lineEdit->text().toDouble()));
	
}

