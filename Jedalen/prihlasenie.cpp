﻿#include "prihlasenie.h"
#include <QtWidgets/QMainWindow>
#include "ui_jedalen.h"
#include <QFile>
#include <QTextStream>
#include <QFileDialog>
#include <QListWidget>
#include <QStringList>
#include <string>
#include <qmessagebox.h>
#include <QListWidgetItem>


Prihlasenie::Prihlasenie(QWidget * parent) : QDialog(parent) {
	ui.setupUi(this);
	setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint);

	QString fileName = QFileDialog::getOpenFileName(this, "Otvorit subor", "", "");

	QFile data(fileName);
	QTextStream input(&data);
	if (!data.open(QFile::ReadOnly))     //kontrola otvorenia suboru
	{
		return;
	}
	for (int i = 1; !input.atEnd(); i++)
	{
		mena << input.readLine();
		pohlavia << input.readLine();
		peniaze << input.readLine();
		hesla << input.readLine();
	}

	ui.comboBox->addItems(mena);

}

Prihlasenie::~Prihlasenie() {
	
}

void Prihlasenie::click1()              //prihlasenie
{
	QMessageBox mbox;

	vyberMeno = ui.comboBox->currentText();
	int indexMeno = mena.indexOf(vyberMeno);
	QString vyberHeslo = ui.lineEdit->text();
	int indexHeslo = hesla.indexOf(vyberHeslo);


	indexPouzivatel = ui.comboBox->currentIndex();
	
	if (indexMeno == indexHeslo)
	{
		mbox.setText("prihlasenie uspesne!");
		mbox.exec();
		close();
	}
	else
	{
		mbox.setText("Zle meno alebo heslo pouzivatela!");
		mbox.exec();
		close();
	}
}
